# LeNet-5 Implementation Using Pytorch  

This project implements LeNet-5 on CIFAR-10 dataset.

## Dataset
`CIFAR-10`: [https://www.cs.toronto.edu/~kriz/cifar.html](https://www.cs.toronto.edu/~kriz/cifar.html)

## Testing Environment
* Pytorch version: `1.4.0`
* CUDA version: `10.1`
* Python version: `3.8.2`

## Reference
* `Previous Implementation`: https://github.com/lychengr3x/LeNet-5-Implementation-Using-Pytorch
* `MNISTtools.py`: Adapted by Charles Deledalle from https://gist.github.com/xmfbit. Inspired from http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py which is GPL licensed.

**Note**: If you have trouble reading the jupyter notebook in github, please click [here](https://nbviewer.jupyter.org/urls/api.npoint.io/10adbeda26faeb95ad02)
