import pickle
import os
import numpy as np
import matplotlib as mpl
from glob import glob
from matplotlib import pyplot

"""
Adapted by Charles Deledalle from https://gist.github.com/xmfbit
Inspired from http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py
which is GPL licensed.
"""

"""
Modified by Erdem SAVASCI to make code work on CIFAR-10 dataset
"""


def unpickle(file):
    with open(file, 'rb') as fo:
        return pickle.load(fo, encoding='bytes')


def get_batches(batch_path):
    batch_dicts = []
    files = glob(batch_path)
    for batch_file in files:
        batch_dict = unpickle(batch_file)
        batch_dicts.append(batch_dict)
    batch_size = len(batch_dicts)
    print('Batch size: ' + str(len(batch_dicts)))

    return batch_dicts


def load(dataset="training", path=None):
    """
    Import either the training or testing CIFAR-10 data set.
    It returns a pair with the first element being the collection of
    images stacked in columns and the second element being a vector
    of corresponding labels from 0 to 9.

    Arguments:
        dataset (string, optional): either "training" or "testing".
            (default: "training")
        path (string, optional): the path pointing to the CIFAR-10 dataset
            If path=None, it looks successively for the dataset at:
            '/dataset/CIFAR-10'. (default: None)

    Example:
        x, lbl = load(dataset="testing", path="/Folder/for/CIFAR-10")
    """

    if path is None:
        path = './dataset/CIFAR-10/'
    if not os.path.isdir(path):
        raise ValueError("Cannot find dataset at '%s'" % path)

    if dataset == "training":
        file_descriptor = os.path.join(path, 'data_batch_*')
        batch_dicts = get_batches(file_descriptor)

    elif dataset == "testing":
        file_descriptor = os.path.join(path, 'test_batch*')
        batch_dicts = get_batches(file_descriptor)

    else:
        raise ValueError("mode must be 'testing' or 'training'")

    images = np.array(0)
    labels = np.array(0)
    filenames = np.array(0)

    for i in range(0, len(batch_dicts)):
        batch_dict = batch_dicts[i]
        print('Load Progress: ' + batch_dict.get(b'batch_label').decode('UTF-8'))
        if images.size == 1:
            images = batch_dict.get(b'data')
        else:
            images = np.vstack((images, batch_dict.get(b'data')))
        if labels.size == 1:
            labels = np.array(batch_dict.get(b'labels'))
        else:
            labels = np.vstack((labels, np.array(batch_dict.get(b'labels'))))
        if filenames.size == 1:
            filenames = np.array(batch_dict.get(b'filenames'))
        else:
            filenames = np.vstack((filenames, np.array(batch_dict.get(b'filenames'))))

    images = np.moveaxis(images, 0, -1)
    labels = labels.astype(int)

    return images, labels


def show(image):
    """
    Render a given CIFAR-10 image provided as a column vector.

    Arguments:
        image (array): an array of shape (32*32) or (32, 32) representing a
            rgb image of size 32 x 32. Values are expected to be in the
            range [0, 1].

    Example:
        x, lbl = load(dataset="training", path="/dataset/CIFAR-10")
        show(x[:, 0])
    """

    rows = 32
    cols = 32
    if image.shape[0] != 3 * rows * cols and image.shape[0] * image.shape[1] != 3 * rows * cols:
        raise Exception("the input is not an CIFAR-10 image.")

    fig = pyplot.figure()
    ax = fig.add_subplot(1, 1, 1)
    image = image.reshape(rows, cols)
    imgplot = ax.imshow(image, cmap=mpl.cm.Greys)
    imgplot.set_interpolation('nearest')
    ax.xaxis.set_ticks_position('top')
    ax.yaxis.set_ticks_position('left')
    pyplot.show()
