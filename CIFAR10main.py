import pickle
import os
import numpy as np
from glob import glob
import argparse
import platform
import time
import torch
import torch.nn as nn
import torch.nn.functional as f
import matplotlib.pyplot as plt

"""
Adapted by Charles Deledalle from https://gist.github.com/xmfbit
Inspired from http://abel.ee.ucla.edu/cvxopt/_downloads/mnist.py
which is GPL licensed.
"""

"""
Modified by Erdem SAVASCI to make code work on CIFAR-10 dataset
Python Version: 3.8.2
"""


# This class is used optional by the arguments that user pass. Other LeNet class is primarily used in the project.
class LeNetV2(nn.Module):

    # network structure
    def __init__(self, activation_func):
        super(LeNetV2, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, 5, [2, 2], [2, 2])
        self.conv2 = nn.Conv2d(32, 32, 5, [2, 2], [2, 2])
        self.conv3 = nn.Conv2d(32, 64, 5, [2, 2], [2, 2])
        self.fc1 = nn.Linear(64 * 4 * 4, 64)
        self.fc2 = nn.Linear(64, 10)
        self.activation_func = activation_func

    def forward(self, x):
        """
        One forward pass through the network.

        Args:
            x: input
        """
        x = f.max_pool2d(self.activation_func(self.conv1(x)), (3, 3), (1, 1), (1, 1))
        x = f.avg_pool2d(self.activation_func(self.conv2(x)), (3, 3), (1, 1), (1, 1))
        x = f.avg_pool2d(self.activation_func(self.conv3(x)), (3, 3), (1, 1), (1, 1))
        x = x.view(-1, self.num_flat_features(x))
        x = self.activation_func(self.fc1(x))
        x = self.activation_func(self.fc2(x))
        return x

    def num_flat_features(self, x):
        """
        Get the number of features in a batch of tensors `x`.
        """
        size = x.size()[1:]
        return np.prod(size)


class LeNet(nn.Module):

    # network structure
    def __init__(self, activation_func):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)
        self.activation_func = activation_func

    def forward(self, x):
        """
        One forward pass through the network.

        Args:
            x: input
        """
        x = f.max_pool2d(self.activation_func(self.conv1(x)), (2, 2))
        x = f.max_pool2d(self.activation_func(self.conv2(x)), (2, 2))
        x = x.view(-1, self.num_flat_features(x))
        x = self.activation_func(self.fc1(x))
        x = self.activation_func(self.fc2(x))
        x = self.fc3(x)
        return x

    def num_flat_features(self, x):
        """
        Get the number of features in a batch of tensors `x`.
        """
        size = x.size()[1:]
        return np.prod(size)


def backprop_deep(optimizer, train_params, test_params, net_param, t_param, b=5, device=torch.device('cpu'),
                  activation_func='relu', model_version=1):
    """
    Backprop.

    Args:
        optimizer: SGD Optimizer
        train_params: training parameters (images_train, labels_train)
        test_params: test parameters (images_test, labels_test)
        net_param: neural network
        t_param: number of epochs
        b: size of one mini batch
        device: Device
    """
    images_param_train = train_params[0]
    labels_param_train = train_params[1]
    images_param_test = test_params[0]
    labels_param_test = test_params[1]

    # Set size
    n = images_param_train.size()[0]
    # Minibatch count
    nb = n // b
    rem = n % b
    last_batch_is_in_different_size = rem > 0
    print('Mini Batch Count: ' + str(nb))
    print('Mini Batch Size: ' + str(b))
    criterion = nn.CrossEntropyLoss()

    if last_batch_is_in_different_size:
        nb = nb + 1

    train_loss_history = []
    test_loss_history = []
    train_acc_history = []
    test_acc_history = []

    for epoch in range(t_param):
        running_loss = 0.0
        train_loss = 0.0
        train_acc = 0.0
        test_loss = 0.0
        test_acc = 0.0

        shuffled_indices = np.random.permutation(nb - 1 if last_batch_is_in_different_size else nb)
        for k in range(nb):
            # Extract k-th minibatch from images_train_param and labels_train_param
            if k == nb - 1 and last_batch_is_in_different_size:
                minibatch_indices = range((max(shuffled_indices) + 1) * b, ((max(shuffled_indices) + 1) * b) + rem)
            else:
                minibatch_indices = range(shuffled_indices[k] * b, (shuffled_indices[k] + 1) * b)

            inputs = images_param_train[minibatch_indices]
            labels = labels_param_train[minibatch_indices]

            # Forward propagation
            outputs = net_param(inputs).to(device)

            # Error evaluation
            loss = criterion(outputs, labels)

            # Initialize the gradients to zero
            optimizer.zero_grad()

            # Back propagation
            loss.backward()

            # Parameter update
            optimizer.step()

            # Print averaged loss per minibatch every 'b' mini-batches
            # Compute and print statistics
            running_loss += loss.item()
            if k % nb == nb - 1:
                train_loss = running_loss / nb
                print('[%d, %5d] Train Loss: %.3f' %
                      (epoch + 1, k + 1, train_loss))

                train_acc = float(100 * (labels == outputs.max(1)[1]).float().mean().cpu())
                print('Train Accuracy: %.3f' % train_acc)

                test_loss, test_acc = backprop_deep_continued(epoch, images_param_test, labels_param_test, net_param,
                                                              device)

        train_loss_history.append(train_loss)
        train_acc_history.append(train_acc)
        test_loss_history.append(test_loss)
        test_acc_history.append(test_acc)

    # Save figures
    if not os.path.exists('./output/'):
        os.mkdir('./output/')
    fig = plt.figure()
    plt.plot(train_loss_history, label='Train Loss')
    plt.plot(test_loss_history, label='Test Loss')
    plt.legend()
    plt.savefig('./output/' + activation_func + '_v' + str(model_version) + '_loss_figure.png', dpi=fig.dpi)
    fig = plt.figure()
    plt.plot(train_acc_history, label='Train Accuracy')
    plt.plot(test_acc_history, label='Test Accuracy')
    plt.legend()
    plt.savefig('./output/' + activation_func + '_v' + str(model_version) + '_acc_figure.png', dpi=fig.dpi)


def backprop_deep_continued(epoch, images_param, labels_param, net_param, device):
    """
    Backprop.

    Args:
        images_param: test samples
        labels_param: labels of samples
        net_param: neural network
    """
    with torch.no_grad():
        criterion = nn.CrossEntropyLoss()
        outputs = net_param(images_param).to(device)
        loss = criterion(outputs, labels_param)
        running_loss = loss.item()
        print('[%d, %5d] Test Loss: %.3f' %
              (epoch + 1, len(images_param), running_loss))

        test_acc = float(100 * (labels_param == outputs.max(1)[1]).float().mean().cpu())
        print('Test Accuracy: %.3f' % test_acc)

        return [running_loss, test_acc]


def unpickle(file):
    with open(file, 'rb') as fo:
        return pickle.load(fo, encoding='bytes')


def normalize_cifar_10_images(x):
    """
    Args:
        x: data
    """
    x_norm = x.astype(np.float32)
    return x_norm * 2 / 255 - 1


def get_batches(batch_path, mode):
    batch_dicts = []
    files = glob(batch_path)
    for batch_file in files:
        batch_dict = unpickle(batch_file)
        batch_dicts.append(batch_dict)
    print('(' + mode + ') Batch size: ' + str(len(batch_dicts)))

    return batch_dicts


def main(path, mode, activation_func, epoch_num, use_gpu_if_available, batch_size, model_version):
    print(f'Pytorch version: {torch.__version__}')
    print(f'cuda version: {torch.version.cuda}')
    print(f'Python version: {platform.python_version()}')

    device = torch.device('cuda:0' if use_gpu_if_available and torch.cuda.is_available() else 'cpu')
    print('Device: ' + device.type)
    print('LeNet Implementation Version: {}'.format(model_version))

    if path is None:
        path = './dataset/CIFAR-10/'
    if not os.path.isdir(path):
        raise ValueError("Cannot find dataset at '%s'" % path)

    if mode == "train":
        file_descriptor = os.path.join(path, 'data_batch_*')
        batch_dicts = get_batches(file_descriptor, 'train')
        file_descriptor = os.path.join(path, 'test_batch*')
        batch_dicts.append(get_batches(file_descriptor, 'test')[0])

    elif mode == "test":
        file_descriptor = os.path.join(path, 'test_batch*')
        batch_dicts = get_batches(file_descriptor, 'test')

    else:
        raise ValueError("mode must be 'test' or 'train'")

    images_train = np.array(0)
    labels_train = np.array(0)
    filenames_train = np.array(0)
    images_test = np.array(0)
    labels_test = np.array(0)
    filenames_test = np.array(0)

    for i in range(0, len(batch_dicts)):
        batch_dict = batch_dicts[i]
        batch_label = batch_dict.get(b'batch_label').decode('UTF-8')
        print('Load Progress: ' + batch_label)
        if 'train' in batch_label:
            if images_train.size == 1:
                images_train = batch_dict.get(b'data')
            else:
                images_train = np.vstack((images_train, batch_dict.get(b'data')))
            if labels_train.size == 1:
                labels_train = np.array(batch_dict.get(b'labels'))
            else:
                labels_train = np.append(labels_train, np.array(batch_dict.get(b'labels')))
            if filenames_train.size == 1:
                filenames_train = np.array(batch_dict.get(b'filenames'))
            else:
                filenames_train = np.append(filenames_train, np.array(batch_dict.get(b'filenames')))
        else:
            if images_test.size == 1:
                images_test = batch_dict.get(b'data')
            else:
                images_test = np.vstack((images_test, batch_dict.get(b'data')))
            if labels_test.size == 1:
                labels_test = np.array(batch_dict.get(b'labels'))
            else:
                labels_test = np.append(labels_test, np.array(batch_dict.get(b'labels')))
            if filenames_test.size == 1:
                filenames_test = np.array(batch_dict.get(b'filenames'))
            else:
                filenames_test = np.append(filenames_test, np.array(batch_dict.get(b'filenames')))

    if mode == "train":
        images_train = np.moveaxis(images_train, 0, -1)
        labels_train = labels_train.astype(int)
        images_train = normalize_cifar_10_images(images_train)
        images_test = np.moveaxis(images_test, 0, -1)
        labels_test = labels_test.astype(int)
        images_test = normalize_cifar_10_images(images_test)

        images_train = images_train.reshape([32, 32, 3, 50000])
        images_test = images_test.reshape([32, 32, 3, 10000])

        images_train = np.moveaxis(images_train, (2, 3), (1, 0))
        images_test = np.moveaxis(images_test, (2, 3), (1, 0))

        images_train = torch.from_numpy(images_train)
        labels_train = torch.from_numpy(labels_train)
        images_test = torch.from_numpy(images_test)
        labels_test = torch.from_numpy(labels_test)
    else:
        images_test = np.moveaxis(images_test, 0, -1)
        labels_test = labels_test.astype(int)
        images_test = normalize_cifar_10_images(images_test)

        images_test = images_test.reshape([32, 32, 3, 10000])

        images_test = np.moveaxis(images_test, (2, 3), (1, 0))

        images_test = torch.from_numpy(images_test)
        labels_test = torch.from_numpy(labels_test)

    if device.type == 'cpu':
        if activation_func == 'relu':
            if model_version == 1:
                net = LeNet(f.relu)
            else:
                net = LeNetV2(f.relu)
        elif activation_func == 'tanh':
            if model_version == 1:
                net = LeNet(torch.tanh)
            else:
                net = LeNetV2(torch.tanh)
        elif activation_func == 'sigmoid':
            if model_version == 1:
                net = LeNet(torch.sigmoid)
            else:
                net = LeNetV2(torch.sigmoid)
        else:
            activation_func = 'relu'
            if model_version == 1:
                net = LeNet(f.relu)
            else:
                net = LeNetV2(f.relu)

        print(net)

        for name, param in net.named_parameters():
            print(name, param.size(), param.requires_grad)

        if mode == "train":
            # gamma: step size
            # rho: momentum
            gamma = .001        # Tested
            rho = .9
            optimizer = torch.optim.SGD(net.parameters(), lr=gamma, momentum=rho)

            net.train(True)

            start = time.time()
            backprop_deep(optimizer, [images_train, labels_train], [images_test, labels_test], net, t_param=epoch_num,
                          b=batch_size if batch_size > 0 else 32, device=device, activation_func=activation_func,
                          model_version=model_version)
            end = time.time()
            print(f'It takes {end - start:.6f} seconds for training.')

            if not os.path.exists('./output/'):
                os.mkdir('./output/')

            torch.save(net.state_dict(), './output/' + activation_func + '_v' + str(model_version) + '_model.pth')
            torch.save(optimizer.state_dict(), './output/' + activation_func + '_v' + str(model_version)
                       + '_optimizer.pth')
        else:
            net.load_state_dict(torch.load('./output/' + activation_func + '_v' + str(model_version)
                                + '_model.pth'))

            net.eval()

            outputs = net(images_test)

            print('Accuracy: %.3f' %
                  float(100 * (labels_test == outputs.max(1)[1].cpu()).float().mean()))

    else:
        if activation_func == 'relu':
            if model_version == 1:
                net_gpu = LeNet(f.relu)
            else:
                net_gpu = LeNetV2(f.relu)
        elif activation_func == 'tanh':
            if model_version == 1:
                net_gpu = LeNet(torch.tanh)
            else:
                net_gpu = LeNetV2(torch.tanh)
        elif activation_func == 'sigmoid':
            if model_version == 1:
                net_gpu = LeNet(torch.sigmoid)
            else:
                net_gpu = LeNetV2(torch.sigmoid)
        else:
            activation_func = 'relu'
            if model_version == 1:
                net_gpu = LeNet(f.relu)
            else:
                net_gpu = LeNetV2(f.relu)

        print(net_gpu)

        for name, param in net_gpu.named_parameters():
            print(name, param.size(), param.requires_grad)

        if mode == "train":
            # gamma: step size
            # rho: momentum
            gamma = .001        # Tested
            rho = .9
            optimizer = torch.optim.SGD(net_gpu.parameters(), lr=gamma, momentum=rho)

            net_gpu = net_gpu.to(device)

            images_gpu_train = images_train.to(device)  # [:100]
            labels_gpu_train = labels_train.to(device, dtype=torch.int64)  # [:100]
            images_gpu_test = images_test.to(device)  # [:100]
            labels_gpu_test = labels_test.to(device, dtype=torch.int64)  # [:100]

            net_gpu.train(True)

            start = time.time()
            backprop_deep(optimizer, [images_gpu_train, labels_gpu_train], [images_gpu_test, labels_gpu_test], net_gpu,
                          t_param=epoch_num, b=batch_size if batch_size > 0 else 32, device=device,
                          activation_func=activation_func, model_version=model_version)
            end = time.time()
            print(f'It takes {end - start:.6f} seconds for training.')

            if not os.path.exists('./output/'):
                os.mkdir('./output/')

            torch.save(net_gpu.state_dict(), './output/' + activation_func + '_v' + str(model_version) + '_model.pth')
            torch.save(optimizer.state_dict(), './output/' + activation_func + '_v' + str(model_version)
                       + '_optimizer.pth')

            del images_gpu_train
            del labels_gpu_train
            del images_gpu_test
            del labels_gpu_test
            torch.cuda.empty_cache()
        else:
            net_gpu.load_state_dict(torch.load('./output/' + activation_func + '_v' + str(model_version)
                                               + '_model.pth'))
            net_gpu = net_gpu.to(device)

            images_gpu_test = images_test.to(device)
            labels_gpu_test = labels_test.to(device)

            net_gpu.eval()

            outputs_gpu = net_gpu(images_gpu_test).to(device)

            print('Accuracy: %.3f' %
                  float(100 * (labels_gpu_test == outputs_gpu.max(1)[1].cuda()).float().mean()))

            del images_gpu_test
            del labels_gpu_test
            torch.cuda.empty_cache()

    exit(0)


def arg2bool(arg):
    if isinstance(arg, bool):
        return arg
    elif arg.lower() in ('yes', 'true', 'y', 't', 1):
        return True
    elif arg.lower() in ('no', 'false', 'n', 'f', 0):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    user_path = './dataset/CIFAR-10/'
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', '-m', type=str, default='train')
    parser.add_argument('--activation_func', '-a', type=str, default='relu')
    parser.add_argument('--epoch_num', '-e', type=int, default=100)
    parser.add_argument('--use_gpu_if_available', '-g', type=arg2bool, const=True, nargs='?', default=True)
    parser.add_argument('--batch_size', '-b', type=int, default=0)
    parser.add_argument('--model_version', '-v', type=int, default=1)
    args = parser.parse_args()
    args = vars(args)
    user_mode = args['mode']
    activation_func = args['activation_func']
    epoch_num = args['epoch_num']
    use_gpu_if_available = args['use_gpu_if_available']
    batch_size = args['batch_size']
    model_version = args['model_version']

    main(user_path, user_mode, activation_func, epoch_num, use_gpu_if_available, batch_size, model_version)
